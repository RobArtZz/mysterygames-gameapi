package eu.mysterygames.gameapi;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;

/**
 * @author RobArtZz
 * 28.04.2020 21:28
 */

public class GameAPI extends JavaPlugin {
    private static GameAPI instance;
    private JedisPoolConfig poolConfig;
    private JedisPool pool;

    @Override
    public void onEnable() {
        super.onEnable();
        initialize();
        setupRedis();
    }

    private void setupRedis() {
        ClassLoader previous = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(Jedis.class.getClassLoader());

        poolConfig = new JedisPoolConfig();
        poolConfig.setMaxTotal(128);
        poolConfig.setTestOnBorrow(true);
        poolConfig.setTestOnReturn(true);
        poolConfig.setTestWhileIdle(true);
        poolConfig.setMinEvictableIdleTimeMillis(Duration.ofSeconds(60).toMillis());
        poolConfig.setTimeBetweenEvictionRunsMillis(Duration.ofSeconds(30).toMillis());
        poolConfig.setNumTestsPerEvictionRun(3);
        poolConfig.setBlockWhenExhausted(true);

        pool = new JedisPool(poolConfig, "localhost");

        Thread.currentThread().setContextClassLoader(previous);
    }

    private void initialize() {
        instance = this;
    }

    @Override
    public void onDisable() {
        super.onDisable();
        pool.close();
    }


    public JedisPool getPool() {
        return pool;
    }

    public static GameAPI getInstance() {
        return instance;
    }

    public static String getPrefix(String chatColor, String name) {
        return "§7┃ " + chatColor + name + " §7» ";
    }

    public static String getPrefix() {
        return getPrefix(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD.toString(), "MysteryGames");
    }
    public static String getNoPermissionMessage() {
        return "§cDazu bist du nicht berechtigt!";
    }

    public static String getNoPlayerMessage() {
        return "§cDu musst ein Spieler sein um dies auszuführen!";
    }

    public static String getCommandSyntax() {
        return "§7Syntax: %s";
    }
}
