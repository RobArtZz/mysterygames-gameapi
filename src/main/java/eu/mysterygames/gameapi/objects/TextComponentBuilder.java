package eu.mysterygames.gameapi.objects;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * @author RobArtZz
 * 28.04.2020 22:00
 */
public class TextComponentBuilder {

    private TextComponent tc;

    public TextComponentBuilder() {
        this.tc = new TextComponent();
    }

    public TextComponentBuilder(String text) {
        this.tc = new TextComponent(text);
    }

    public TextComponentBuilder(TextComponent tc) {
        this.tc = tc;
    }

    public TextComponentBuilder setText(String text) {
        this.tc.setText(text);
        return this;
    }

    public TextComponentBuilder setBold(boolean bold) {
        this.tc.setBold(bold);
        return this;
    }

    public TextComponentBuilder setItalic(boolean italic) {
        this.tc.setItalic(italic);
        return this;
    }

    public TextComponentBuilder setUnderlined(boolean underlined) {
        this.tc.setUnderlined(underlined);
        return this;
    }

    public TextComponentBuilder setColor(ChatColor color) {
        this.tc.setColor(color);
        return this;
    }

    public TextComponentBuilder addExtra(String extra) {
        this.tc.addExtra(extra);
        return this;
    }

    public TextComponentBuilder addExtra(TextComponent tc) {
        this.tc.addExtra(tc);
        return this;
    }

    public TextComponentBuilder addExtra(TextComponentBuilder tcb) {
        this.tc.addExtra(tcb.build());
        return this;
    }

    public TextComponentBuilder setClickEvent(ClickEvent clickEvent) {
        this.tc.setClickEvent(clickEvent);
        return this;
    }

    public TextComponentBuilder setHoverEvent(HoverEvent hoverEvent) {
        this.tc.setHoverEvent(hoverEvent);
        return this;
    }

    public TextComponentBuilder duplicate() {
        return new TextComponentBuilder(tc);
    }

    public TextComponent build() {
        return this.tc;
    }
}
