package eu.mysterygames.gameapi.objects;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;

/**
 * @author RobArtZz
 * 28.04.2020 22:01
 */
public class ItemBuilder {
    private final ItemStack item;

    public ItemBuilder(Material material) {
        this.item = new ItemStack(material);
        this.item.setAmount(1);
    }

    public ItemBuilder setDisplayName(String name) {
        ItemMeta m = this.item.getItemMeta();
        if (m != null) {
            m.setDisplayName(name);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.item.setAmount(amount);
        return this;
    }

    public ItemBuilder setLore(String... lore) {
        ItemMeta m = this.item.getItemMeta();
        if (m != null) {
            m.setLore(Arrays.asList(lore));
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder addEnchant(Enchantment enchantment, int level) {
        this.item.addUnsafeEnchantment(enchantment, level);
        return this;
    }

    public ItemBuilder addFlag(ItemFlag itemFlag) {
        ItemMeta m = this.item.getItemMeta();
        if (m != null) {
            m.addItemFlags(itemFlag);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder setUnbreakable(boolean value) {
        ItemMeta m = this.item.getItemMeta();
        if(m != null) {
            m.spigot().setUnbreakable(value);
        }
        return this;
    }

    public ItemBuilder setSkullOwner(String name) {
        SkullMeta m = (SkullMeta)this.item.getItemMeta();
        if (m != null) {
            m.setOwner(name);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder setLeatherColor(Color color) {
        LeatherArmorMeta m = (LeatherArmorMeta)this.item.getItemMeta();
        if (m != null) {
            m.setColor(color);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder setBookAuthor(String author) {
        BookMeta m = (BookMeta)this.item.getItemMeta();
        if (m != null) {
            m.setAuthor(author);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder setBookContent(String... pages) {
        BookMeta m = (BookMeta)this.item.getItemMeta();
        if (m != null) {
            m.setPages(pages);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder setBookTitle(String title) {
        BookMeta m = (BookMeta)this.item.getItemMeta();
        if (m != null) {
            m.setTitle(title);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public ItemBuilder setPotionType(PotionEffectType type) {
        PotionMeta m = (PotionMeta)this.item.getItemMeta();
        if (m != null) {
            m.setMainEffect(type);
        }

        this.item.setItemMeta(m);
        return this;
    }

    public Material getMaterial() {
        return this.item.getType();
    }

    public ItemStack getItem() {
        return this.item;
    }

    public ItemStack build() {
        return this.item;
    }
}
